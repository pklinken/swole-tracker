export const formattedDateHelper = function(date: number): string {
  return new Date(date).toLocaleDateString();
};

export const groupBy = function<TItem>(
  xs: TItem[],
  key: string
): { [key: string]: TItem[] } {
  return xs.reduce(function(rv: { [key: string]: TItem[] }, x: any) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};
