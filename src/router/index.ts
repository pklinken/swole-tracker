import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Search from "@/components/Search.vue";
import ExerciseOverview from "@/components/ExerciseOverview.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Search",
    component: Search,
  },
  {
    path: "/exercise/:id",
    name: "Exercise",
    component: ExerciseOverview,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
