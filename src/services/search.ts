import { storeService } from "./store";
// import { InExerciseDataPoint } from '@/models/ExerciseDatapoint';
import { ISearchResult } from "@/models/SearchResult";

class SearchService {
  search(searchTerm: string): Promise<Array<ISearchResult>> {
    return new Promise((resolve, _reject) => {
      // Get le data
      console.log(`Searching for ${searchTerm}`);
      const returnValue: Array<ISearchResult> = [];

      const matchingExercises = storeService.searchExercisesByName(searchTerm);
      matchingExercises.forEach((ex) => {
        returnValue.push({
          exercise: ex,
          datapoints: storeService.getDataPointsForId(ex.id),
        });
      });
      // return returnValue;
      // Filter for searchTerm
      // exerciseData = exerciseData
      //     .filter((dataPoint) => dataPoint.name.toLowerCase().includes(searchTerm.toLowerCase()));
      // // Group searchresult by name
      // const returnValue: Array<ISearchResult> = [];
      // exerciseData.reduce((acc: Array<ISearchResult>, curr: InExerciseDataPoint) => {
      //     const match = acc.find((sr) => sr.name == curr.name);
      //     if(match === undefined) {
      //         acc.push({
      //             name: curr.name,
      //             datapoints: [curr]
      //         });
      //     } else {
      //         match.datapoints = match.datapoints.concat(curr);
      //     }
      //     return acc;
      // }, returnValue);
      resolve(returnValue);
    });
  }
}

export const searchService = new SearchService();
