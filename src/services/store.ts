import { InExerciseDataPoint } from "@/models/ExerciseDatapoint";
import { InExercise, Exercise } from "@/models/Exercise";

export const datapointStorageKey = "swole-tracker-datapoints";
export const exerciseStorageKey = "swole-tracker-exercises";

class StoreService {
  createDatapoint(datapoint: InExerciseDataPoint) {
    const currentData = this.getDatapoints();
    // This doesn't check if exercise already exists
    datapoint.id = this.nextDatapointId();
    currentData.push(datapoint);
    localStorage.setItem(datapointStorageKey, JSON.stringify(currentData));
  }

  getDatapoints(): Array<InExerciseDataPoint> {
    const currentDataString = localStorage.getItem(datapointStorageKey);
    return currentDataString !== null ? JSON.parse(currentDataString) : [];
  }

  getDataPointsForId(exerciseId: number): Array<InExerciseDataPoint> {
    return this.getDatapoints().filter((dp) => dp.exerciseId == exerciseId);
  }

  createExercise(exercise: InExercise): InExercise | null {
    if (!this.getExercise(exercise.id)) {
      // If exercise does not already exist
      const exercises = this.getExercises();
      const createdExercise = new Exercise(
        this.nextExerciseId(),
        exercise.name
      );
      exercises.push(createdExercise);
      localStorage.setItem(exerciseStorageKey, JSON.stringify(exercises));
      return createdExercise;
    } else return null;
  }

  searchExercisesByName(name: string): Array<InExercise> {
    return this.getExercises().filter((e) =>
      e.name.toLowerCase().includes(name.toLowerCase())
    );
  }

  getExercise(exerciseId: number): InExercise | undefined {
    return this.getExercises().find((e) => e.id == exerciseId);
  }

  getExercises(): Array<InExercise> {
    const currentDataString = localStorage.getItem(exerciseStorageKey);
    return currentDataString !== null ? JSON.parse(currentDataString) : [];
  }

  private nextExerciseId(): number {
    const exercises = this.getExercises();
    let highestId = 1;
    exercises.forEach((ex) => {
      if (ex.id > highestId) highestId = ex.id;
    });
    return highestId + 1;
  }

  private nextDatapointId(): number {
    const datapoints = this.getDatapoints();
    let highestId = 1;
    datapoints.forEach((dp) => {
      if (dp.id > highestId) highestId = dp.id;
    });
    return highestId + 1;
  }
}

export const storeService = new StoreService();
