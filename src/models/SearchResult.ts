import { InExerciseDataPoint } from "./ExerciseDatapoint";
import { InExercise } from "./Exercise";

export interface ISearchResult extends Object {
  exercise: InExercise;
  datapoints: Array<InExerciseDataPoint>;
}
