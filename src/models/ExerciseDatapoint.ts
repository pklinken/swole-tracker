export interface InExerciseDataPoint {
  id: number;
  date: number;
  exerciseId: number;
  weight: number;
  reps: number;
  note: string;
}

export class ExerciseDatapoint implements InExerciseDataPoint {
  id: number;
  date: number;
  exerciseId: number;
  weight: number;
  reps: number;
  note: string;

  constructor(
    id = -1,
    date = Date.now(),
    exerciseId = -1,
    weight = 0,
    reps = 0,
    note = ""
  ) {
    this.id = id;
    this.date = date;
    this.exerciseId = exerciseId;
    this.weight = weight;
    this.reps = reps;
    this.note = note;
  }
}

export function getMostRecentSessions(
  datapoints: InExerciseDataPoint[]
): Array<InExerciseDataPoint> {
  return (
    datapoints
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      .map((dp: any) => {
        return dp;
      })
      .sort((a: InExerciseDataPoint, b: InExerciseDataPoint) => b.date - a.date)
  );
}
