export interface InExercise {
  id: number;
  name: string;
}

export class Exercise implements InExercise {
  id: number;
  name: string;

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}
